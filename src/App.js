import React, {useState} from "react"
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import {Layout} from "antd";
import Navigation from "./components/Navigation";

import './App.css'
import Phonology from "./features/phonology/Phonology";
import VocabularyGrammar from "./features/vocabulary-grammar/VocabularyGrammar";
import ReadingComprehension from "./features/reading-comprehension/ReadingComprehension";
import Writing from "./features/writing/Writing";
import Question from "./features/question/Question";
import BreadCrumbs from "./components/BreadCrumb";
import VocabularyType from "./features/vocabulary-type/VocabularyType";
import Vocabulary from "./features/vocabulary/Vocabulary";
import Home from "./features/home/Home";

const {Header, Sider, Content} = Layout;

function App() {
    return (
        <Router>
            <Layout className={"app"}>
                <Sider breakpoint="xxl" width={250}>
                    <Navigation/>
                </Sider>
                <Layout>
                    <Header style={{padding: '0 24px 24px'}}>
                        <BreadCrumbs/>
                    </Header>
                    <Content style={{
                        padding: 15,
                        minHeight: 280,
                    }}>
                        <Switch>
                            <Route path={"/phonology"}>
                                <Phonology/>
                            </Route>
                            <Route path={"/vocabulary-grammar"}>
                                <VocabularyGrammar/>
                            </Route>
                            <Route path={"/reading-comprehension"}>
                                <ReadingComprehension/>
                            </Route>
                            <Route path={"/writing"}>
                                <Writing/>
                            </Route>
                            <Route path={"/question"}>
                                <Question/>
                            </Route>
                            <Route path={"/vocabulary-type"}>
                                <VocabularyType/>
                            </Route>
                            <Route path={"/vocabulary"}>
                                <Vocabulary/>
                            </Route>
                            <Route path={"/"}>
                                <Home/>
                            </Route>
                        </Switch>
                    </Content>
                </Layout>
            </Layout>
        </Router>
    );
}

export default App;
